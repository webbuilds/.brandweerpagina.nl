<?php

namespace App\Persmonitor;

use Cache;
use Curl\Curl;

class ApiController
{
    private $apiUrl;

    public function __construct()
    {
        $this->apiUrl = config('services.persmonitor.eindpoint');
    }

	function request($apiRequest, $method, $data = [])
	{
		$apiData = [

		];

        $curl = new Curl();
        $curl->setOpt( CURLOPT_SSL_VERIFYPEER, false );
        $curl->setOpt( CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $curl->setUserAgent(data_get($_SERVER,'HTTP_USER_AGENT'));
        $curl->setHeader( 'Authorization', config('services.persmonitor.key') );
        $curl->setHeader( 'X-Forwarded-For', $_SERVER['REMOTE_ADDR'] );
        $curl->setTimeout(1000);
        $curl->$method( $this->apiUrl . $apiRequest, array_merge( $apiData, $data ) );
		$curl->close();

        return $curl;
	}
}
