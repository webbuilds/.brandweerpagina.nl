<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Http\Request;

class WebController extends Controller
{

    public function index(Request $request)
    {
        $queryFireStation = [
            'fields' => 'name,number,cover,website,twitter,facebook,instagram,region{name},capcodes{capcode,description}'
        ];

        $requestFireStation = $this->api('fire_stations/name/' . current(explode('.', $request->getHost(), 2)), 'get', $queryFireStation);
        if(data_get($requestFireStation,'httpStatusCode') == 200) {

            $fireStation = data_get($requestFireStation, 'response');

            $data = [
                'fireStation' => $fireStation,
                'vehicles' => $this->vehicles(data_get($fireStation,'name'))
            ];

            return view('welcome', $data);
        }
        abort(404);
    }

    public function vehicles($fireStation)
    {
        $queryVehicles = [
            'order_by' => 'name',
            'sort' => 'asc',
            'limit' => 100,
            'fields' => 'name,number,description,region{name,number},vehicle_type{name},fire_station{name},cover',
            'fire_stations' => [
                [
                    'eq' => $fireStation,
                    'param' => 'name'
                ]
            ]
        ];

        $requestVehicles = $this->api('vehicles', 'get', $queryVehicles);
        if(data_get($requestVehicles,'httpStatusCode') == 200) {

            $vehicles = data_get($requestVehicles, 'response');

            return $vehicles;
        }
    }

}
